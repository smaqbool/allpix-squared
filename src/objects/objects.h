#include "DepositedCharge.hpp"
#include "PixelCharge.hpp"
#include "PixelHit.hpp"
#include "PropagatedCharge.hpp"

namespace allpix {
    using OBJECTS = std::tuple<DepositedCharge, PropagatedCharge, PixelCharge, PixelHit>;
}
